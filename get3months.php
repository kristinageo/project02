


<?php

require_once 'database.php';


    $sqlToday = "SELECT * 
    FROM ( SELECT *, (ROW_NUMBER() OVER (PARTITION BY country_id ORDER BY date DESC)) as row_num FROM covid_data ) partitioned_table 
    INNER JOIN countries ON countries.id = partitioned_table.country_id
    WHERE partitioned_table.row_num = 1";//2021-06-03

    $sqlThreeMonths ="SELECT * 
    FROM ( SELECT *, (ROW_NUMBER() OVER (PARTITION BY country_id ORDER BY date DESC)) as row_num FROM covid_data ) partitioned_table 
    INNER JOIN countries ON countries.id = partitioned_table.country_id
    WHERE partitioned_table.row_num = 90";
    $stmt = $pdo->query($sqlToday);
    $stmtThreeMonths = $pdo->query($sqlThreeMonths);
    if($stmt->rowCount()) {
        $dataToday = $stmt->fetch();
        $date = $dataToday['date'];   
    }
    if($stmtThreeMonths->rowCount()) {
      $dataMonthsAgo = $stmtThreeMonths->fetch();
      $dateThreeMonthsAgo = $dataMonthsAgo['date'];   
  }
    $sql = "SELECT sum(confirmed),sum(deaths),sum(recovered),sum(active) FROM covid_data WHERE date BETWEEN '$dateThreeMonthsAgo' AND '$date'";
    $stmtSum = $pdo->query($sql);
    while($country = $stmtSum->fetch()) {
        echo "  <h2 class='text-center mt-3'>Confirmed:</h2>";
        echo "<h3  class='text-center text-info font-weight-bold'>".number_format($country['sum(confirmed)']) ."</h3 >";
        echo "<h2 class='text-center'>Deaths:</h2>";
        echo "<h3  class='text-center text-secondary font-weight-bold'>".number_format($country['sum(deaths)']) ." </h3 >";
        echo "<h2  class='text-center'>Recovered:</h2>";
        echo "<h3  class='text-center text-success font-weight-bold'>".number_format($country['sum(recovered)']) ."</h3 >";
        echo "<h2  class='text-center'>Active:</h2>";
        echo "<h3  class='text-center text-danger font-weight-bold'>".number_format($country['sum(active)']) ."</h3 >";
    

}


?>


