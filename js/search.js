
$(document).ready(function(){
  var choosenTimePeriod='default';
  load_data();
  $.ajax({
        url:"fill.php",
        method: "GET",
        dataType:'html',
        success:function(html)
        { 
          console.log(html);
          $("#searchme").append(html);
               
        }
  });
  $("#choose").change(function(){
    var choosenTimePeriod=$(this).val();
    if(choosenTimePeriod == 'daily' || choosenTimePeriod == 'default'){
      load_data();
    }else if(choosenTimePeriod == 'lastmonth'){
      loadlast_data();
    }else{
      loadthreemonths_data();
    }
  })
  function load_data(){
    $.ajax({
			url:"load.php",
			method:"GET",
      dataType:'html',
			success:function(html)
			{
        $('tbody').html("");
				$('tbody').append(html);
			}
		});
  }
  function loadlast_data(){
    $.ajax({
			url:"loadlast.php",
			method:"GET",
      dataType:'html',
			success:function(html)
			{
        $('tbody').html("");
				$('tbody').append(html);
			}
		});
  }
  function loadthreemonths_data(){
    $.ajax({
			url:"loadthreemonths.php",
			method:"GET",
      dataType:'html',
			success:function(html)
			{
        // console.log(html);
        $('tbody').html("");
				$('tbody').append(html);
			}
		});
  }
  $('#submit').click(function(e){
        e.preventDefault();
        var search = $("#search").val();
        var choosenvalue=$("#choose").val();
        if(choosenvalue == 'daily' || choosenvalue=='default'){
          $.ajax({
            type: "POST",
            url: "search.php",
            data: {search: search},
            success : function(data){
              console.log(data)
              $('#chart').html("");
              $("tbody").html(data)
            }
        });        
        }else if(choosenvalue=='lastmonth'){
          $.ajax({
            type: "POST",
            url: "searchlast.php",
            data: {search: search},
            success : function(data){
              console.log(data)
              $('#chart').html("");
              $("tbody").html(data)
            }
        });
        }else{
          $.ajax({
            type: "POST",
            url: "searchthree.php",
            // dataType: "json",
            data: {search: search},
            success : function(data){
              console.log(data)
              $('#chart').html("");
              $("tbody").html(data)
            }
        });
        }	
    	});
});
// $('select').on('change', function (e) {
//   var optionSelected = $("option:selected", this);
//   var valueSelected = this.value;
//   ....
// });
$('#searchme').on('change click',function(e){
  e.preventDefault();
  var search =  $("option:selected", this).val().toLowerCase();
  console.log(search);
  var choosenvalue=$("#choose").val();
  if(choosenvalue == 'daily' || choosenvalue=='default'){
    $.ajax({
      type: "POST",
      url: "search.php",
      data: {search: search},
      success : function(data){
        $('#chart').html("");
        $("tbody").html(data)
      }
  });        
  }else if(choosenvalue=='lastmonth'){
    $.ajax({
      type: "POST",
      url: "searchlast.php",
      data: {search: search},
      success : function(data){
        $('#chart').html("");
        $("tbody").html(data)
      }
  });
  }else{
    $.ajax({
      type: "POST",
      url: "searchthree.php",
      // dataType: "json",
      data: {search: search},
      success : function(data){
        $('#chart').html("");
        $("tbody").html(data)
      }
  });
  }	
});
