$(document).ready(function() {
    //  $("#chart").CanvasJSChart("");
    var selectOption = 'default';
    console.log(selectOption)
        $("#choose").change(function(){
          selectOption = $("#choose").val();
          $("#chart").html("");
        })
     var table = $("table tbody");
     $('body').on({
        mouseenter: function () {
            // var cellIndex = $(this).index();
        
               $(this).css('cursor','pointer')
               $(this).css('background-color','gray');
        },
        mouseleave: function () {
               $(this).css('cursor','')
               $(this).css('background-color','');
        }
    }, '.row_graph');
     $(document).on('click','.row_graph',function(){
        // $(this).css('cursor','pointer')
        // $(this).css('background-color','red');
         var currentRow = $(this).find("td");
        //  console.log(currentRow);
         id =  currentRow.eq(0).text(),
         country =  currentRow.eq(1).text(),
         confirmed =  parseInt(currentRow.eq(2).text());
         deaths =  parseInt(currentRow.eq(4).text());
         recovered = parseInt(currentRow.eq(5).text());
         active =  parseInt(currentRow.eq(3).text());
         newcases =  parseInt(currentRow.eq(6).text());
         newdeaths = parseInt(currentRow.eq(7).text());
         newrecovered =  parseInt(currentRow.eq(8).text());
         console.log(typeof confirmed);
         var options = {
             title: {
                 text: country           
             },
             data: [              
             {
                 // Change type to "doughnut", "line", "splineArea", etc.
                 type: "column",
                 dataPoints: [
                     { label: "confirmed",  y: confirmed  },
                     { label: "active",  y: active  },
                     { label: "deaths", y: deaths },
                     { label: "recovered", y: recovered  },
                     { label: "newcases",  y: newcases  },
                     { label: "newdeaths",  y: newdeaths  },
                     { label: "newrecovered",  y: newrecovered  }
                 ]
             }
             ]
         }
         // console.log(options);
    
        console.log(selectOption);
        if(selectOption=='default' || selectOption=='daily'){
              options.title.text = 'Covid cases for ' + country +' (daily)';
        }else if(selectOption == 'lastmonth'){
              options.title.text = 'Covid cases for ' + country + ' (last month)';
        }else{
              options.title.text = 'Covid cases for ' + country + ' (three months ago)';
        }
         $("#chart").CanvasJSChart(options);
        //  console.log($("#chart"));
         // table.find('tr').each(function (i) {
         //     var $tds = $(this).find('td'),
         //         id = $tds.eq(0).text(),
         //         country = $tds.eq(1).text(),
         //         confirmed = $tds.eq(2).text();
         //         deaths = $tds.eq(3).text();
         //         recovered = $tds.eq(4).text();
         //         active = $tds.eq(5).text();
         //         newcases = $tds.eq(6).text();
         //         newdeaths = $tds.eq(7).text();
         //         newrecovered = $tds.eq(8).text();
         //     // do something with productId, product, Quantity
             // console.log('ID:' + id
             //       + '\nCountry: ' + country
             //       + '\nConfirmed: ' + confirmed
             //       + '\nDeaths: ' + deaths
             //       +'\nRecovered: ' + recovered
             //       +'\nActive: ' + active
             //       + '\nNewcases: ' + newcases
             //       + '\nNewdeaths: ' + newdeaths
             //       + '\nNewrecovered: ' + newrecovered);
         // });
    
     })
    
    });