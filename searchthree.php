<?php

        require_once 'database.php';
        $keyword = $_POST['search'];
        $sqlToday = "SELECT * 
            FROM ( SELECT *, (ROW_NUMBER() OVER (PARTITION BY country_id ORDER BY date DESC)) as row_num FROM covid_data ) partitioned_table 
            INNER JOIN countries ON countries.id = partitioned_table.country_id
            WHERE partitioned_table.row_num = 1 and slug LIKE '%$keyword%'";//macedonia na primer united 3 
        $stmt = $pdo->query($sqlToday);
        if($stmt->rowCount()){
            while($country = $stmt->fetch()) {//3 
                $todayTotal = $country['confirmed'];
                $todayActive = $country['active'];
                $todayDeaths = $country['deaths'];
                $todayRecovered = $country['recovered'];
                $dateToday = $country['date'];
                $newCases = '/';
                $newDeaths = '/';
                $newRecovered = '/';
                $sqlLastDayOfMonth= "SELECT * FROM covid_data WHERE country_id = {$country['id']} ORDER BY `date` DESC LIMIT 1 OFFSET 90";
                //gi davva resultatite za vcera
                $stmtLastDayOfMonth = $pdo->query($sqlLastDayOfMonth);
                if($stmtLastDayOfMonth->rowCount()) {
                    $dataLastDayOfMonth = $stmtLastDayOfMonth->fetch();
                    $dateLastMonth = $dataLastDayOfMonth['date'];
                    $newCases = $todayTotal - $dataLastDayOfMonth['confirmed'];
                    $newDeaths = $todayDeaths - $dataLastDayOfMonth['deaths'];
                    $newRecovered = $todayRecovered - $dataLastDayOfMonth['recovered'];
                }
                $id = $country['id'];
                $sqlForLastMonth = "SELECT sum(confirmed) as sum_confirmed,sum(deaths) as sum_deaths,
                sum(recovered) as sum_recovered,sum(active)  as sum_active
                FROM covid_data WHERE country_id = $id and date BETWEEN  '$dateLastMonth' AND  '$dateToday'";
                 $stmtSumLastMonth = $pdo->query($sqlForLastMonth);
              
                 if($stmtSumLastMonth->rowCount()) {
                    $dataLastMonth = $stmtSumLastMonth->fetch();
                    // echo $dataLastMonth;
                    $sum_confirmed = $dataLastMonth['sum_confirmed'];
                    // echo $sum_confirmed;
                    $sum_deaths = $dataLastMonth['sum_deaths'];
                    // echo $sum_deaths;
                    $sum_recovered =  $dataLastMonth['sum_recovered'];
                    $sum_active =  $dataLastMonth['sum_active'];
                }
                // echo $sum_confirmed;
                echo "<tr class='row_graph'>
                <td>{$country['country_id']}</td>
                <td>".utf8_encode($country['country']) ."</td>
                <td>{$sum_confirmed}</td>
                <td>{$sum_active}</td>
                <td>{$sum_deaths}</td>
                <td>{$sum_recovered}</td>
                <td>{$newCases}</td>
                <td>{$newDeaths}</td>
                <td>{$newRecovered}</td>
                </tr>";    
            }

        }else{
            echo "No data";
        }
      
?>