<?php

require_once 'database.php';


$sql = "SELECT * 
    FROM ( SELECT *, (ROW_NUMBER() OVER (PARTITION BY country_id ORDER BY date DESC)) as row_num FROM covid_data ) partitioned_table 
    INNER JOIN countries ON countries.id = partitioned_table.country_id
    WHERE partitioned_table.row_num = 1";
        $stmt = $pdo->query($sql);
        while($country = $stmt->fetch()) {
        $todayTotal = $country['confirmed'];
        $todayActive = $country['active'];
        $todayDeaths = $country['deaths'];
        $todayRecovered = $country['recovered'];
        echo "recovered today".$todayRecovered;
        $newCases = '/';
        $newDeaths = '/';
        $newRecovered = '/';

        $sqlYesterday = "SELECT * FROM covid_data WHERE country_id = {$country['id']} ORDER BY `date` DESC LIMIT 1 OFFSET 1";
        $stmtYesterday = $pdo->query($sqlYesterday);
        if($stmtYesterday->rowCount()) {
            $dataYesterday = $stmtYesterday->fetch();

            $newCases = $todayTotal - $dataYesterday['confirmed'];
            $newDeaths = $todayDeaths - $dataYesterday['deaths'];
            $newRecovered = $todayRecovered - $dataYesterday['recovered'];
        }
      
   
        
        echo "<tr class='row_graph'>
        <td>{$country['country_id']}</td>
        <td class='graph_country'>".utf8_encode($country['country']) ."</td>
        <td>{$todayTotal}</td>
        <td>{$todayActive}</td>
        <td>{$todayDeaths}</td>
        <td>{$todayRecovered}</td>
        <td>{$newCases}</td>
        <td>{$newDeaths}</td>
        <td>{$newRecovered}</td>
        </tr>";
        

	}
